.PHONY: help, bash, build, exec, run

APP=diannepat/xtractor
SHORT=xroi

help:
	@echo "Make what?: help, bash, build, exec, run"

bash:
	docker run -it --rm --name ${SHORT} -v ${PWD}:/io --entrypoint /bin/bash ${APP}

build:
	docker build -t ${APP} .

exec:
	docker exec -it ${SHORT} bash

run:
	docker run -it --rm --name ${SHORT} -v ${PWD}:/io ${APP} run-01_IC-06_Russian_Unlearnable.nii 1.26
