FROM bids/base_fsl

RUN mkdir /app /io

COPY xtractor /app/
COPY ATLASES /app/ATLASES

ENTRYPOINT ["/app/xtractor"]
