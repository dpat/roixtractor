#!/bin/bash

if [ $# -lt 3 ]; then
  echo "You must supply a filename (assumes FSL MNI space, 2mm), a threshold value, and an atlas"
  echo "Atlases are currently HO-CB or HCP-MMP1"
  echo "HO-CB is a label atlas constructed from three existing atlases, Harvard-Oxford Cortical,subcortical and a Cerebellar atlas"
  echo "HCP-MMP1 is the Glasser 2016 atlas in 2mm MNI space"
  echo "Note that the HCP-MMP1 atlas was never intended for volumetric analysis like this"
  echo "so results should be taken with a large grain of salt."
  echo "e.g., $0 run-01_IC-06_Russian_Unlearnable.nii 1.26 HO-CB"
  exit 1
fi

img=$1
stat_thresh=$2
atlas=$3

docker run -it --rm -v ${PWD}:/io diannepat/xtractor ${img} ${stat_thresh} ${atlas}
